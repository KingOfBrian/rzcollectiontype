//
//  CollectionTypeSectionTests.swift
//  RZCollectionType
//
//  Created by Brian King on 12/21/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import XCTest
@testable import RZCollectionType

class CollectionTypeSectionTests: XCTestCase {

    func testBasics() {
        let previous: [TestSection] = [
            .Info(name: "Welcome", rowTitle: "Click For More Details"),
            .Group(name: "Boston", rows: [
                Room(identifier:"A", display: "Death Star"),
                Room(identifier:"B", display: "Dagobah"),
                Room(identifier:"C", display: "Yavin"),
                ]),
            .Group(name: "San Francisco", rows: [
                Room(identifier:"D", display: "Stand Up"),
                Room(identifier:"E", display: "Sit Down")
                ]),
            .Footer(state: .More)
        ]
        let current: [TestSection] = [
            // Remove Info
            .Group(name: "Boston", rows: [
                Room(identifier:"A", display: "Death Star (Destroyed)"), // Update
                Room(identifier:"C", display: "Yavin"),    // Move
                Room(identifier:"B", display: "Dagobah"),       // to here
                Room(identifier:"F", display: "Endor"), // Insert
                ]),
            .Group(name: "San Francisco", rows: [
                Room(identifier:"D", display: "Stand Up NOW"), // Update
                Room(identifier:"E", display: "Sit Down")
                ]),
            .Info(name: "Info", rowTitle: "Did You Know?"), // Insert
            .Footer(state: .Loading) // Update
        ]
        let foo = current.objectAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
        print(foo)
        // This does not work.
        let (s, r) = current.calculateSectionedArrayChanges(previous)
        XCTAssertEqual(s.count, 6)
        XCTAssertEqual(r.count, 4)
        XCTAssertEqual(s[0], CollectionTypeChange.Remove(index: 0))
        XCTAssertEqual(s[1], CollectionTypeChange.Remove(index: 3))
        XCTAssertEqual(s[2], CollectionTypeChange.Insert(index: 2))
        XCTAssertEqual(s[3], CollectionTypeChange.Insert(index: 3))
        XCTAssertEqual(s[4], CollectionTypeChange.Update(index: 0))
        XCTAssertEqual(s[5], CollectionTypeChange.Update(index: 1))

// Compiler no-likey:
//        XCTAssert(r[0] == RowChange.RowMove(fromSectionIndex: 0, fromRowIndex: 2, toSectionIndex: 0, toRowIndex: 1))
//        XCTAssertEqual(r[1], RowChange.RowInsert(sectionIndex: 0, rowIndex: 3))
//        XCTAssertEqual(r[2], RowChange.RowUpdate(sectionIndex: 0, rowIndex: 0))
//        XCTAssertEqual(r[3], RowChange.RowUpdate(sectionIndex: 1, rowIndex: 0))

    }
}