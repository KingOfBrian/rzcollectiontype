//
//  RZCollectionTypeTests.swift
//  RZCollectionTypeTests
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import XCTest
@testable import RZCollectionType

class CollectionTypeArrayTests: XCTestCase {

    func testEquatableArrayNoMoves() {
        let previous = [0,4]
        let current = [1,2,3]
        let changes = current.calculateArrayChanges(previous)
        XCTAssert(changes.count == 5)
        XCTAssert(changes[0] == CollectionTypeChange.Remove(index: 0))
        XCTAssert(changes[1] == CollectionTypeChange.Remove(index: 1))
        XCTAssert(changes[2] == CollectionTypeChange.Insert(index: 0))
        XCTAssert(changes[3] == CollectionTypeChange.Insert(index: 1))
        XCTAssert(changes[4] == CollectionTypeChange.Insert(index: 2))

        let changeBack = previous.calculateArrayChanges(current)
        XCTAssert(changeBack.count == 5)
        XCTAssert(changeBack[0] == CollectionTypeChange.Remove(index: 0))
        XCTAssert(changeBack[1] == CollectionTypeChange.Remove(index: 1))
        XCTAssert(changeBack[2] == CollectionTypeChange.Remove(index: 2))
        XCTAssert(changeBack[3] == CollectionTypeChange.Insert(index: 0))
        XCTAssert(changeBack[4] == CollectionTypeChange.Insert(index: 1))
    }
    
    func testEquatableWithMoves() {
        let previous = [3,2,1]
        let current = [1,2,3]
        let changes = current.calculateArrayChanges(previous)
        XCTAssert(changes.count == 2)
        XCTAssert(changes[0] == CollectionTypeChange.Move(from: 0, to: 2))
        XCTAssert(changes[1] == CollectionTypeChange.Move(from: 2, to: 0))

        let changeBack = previous.calculateArrayChanges(current)
        XCTAssert(changeBack.count == 2)
        XCTAssert(changes[0] == CollectionTypeChange.Move(from: 0, to: 2))
        XCTAssert(changes[1] == CollectionTypeChange.Move(from: 2, to: 0))
    }
    
    func testIdentifiableNoMoves() {
        let previous = [Foo(value: 2, key:1), Foo(value: 4, key:2)]
        let current  = [Foo(value: 1, key:1), Foo(value: 2, key:2), Foo(value: 3, key:3)]
        let changes = current.calculateArrayChanges(previous)
        XCTAssert(changes.count == 3)
        XCTAssert(changes[0] == CollectionTypeChange.Insert(index: 2))
        XCTAssert(changes[1] == CollectionTypeChange.Update(index: 0))
        XCTAssert(changes[2] == CollectionTypeChange.Update(index: 1))
    }

    func testIdentifiableShiftedNotMoved() {
        let previous = [Foo(value: 2, key:1), Foo(value: 4, key:2)]
        let current = [                       Foo(value: 2, key:2), Foo(value: 3, key:3)]
        let changes = current.calculateArrayChanges(previous)
        XCTAssert(changes.count == 3)
        XCTAssert(changes[0] == CollectionTypeChange.Remove(index: 0))
        XCTAssert(changes[1] == CollectionTypeChange.Insert(index: 1))
        XCTAssert(changes[2] == CollectionTypeChange.Update(index: 0))
    }

    func testIdentifiableMoved() {
        let previous = [Foo(value: 4, key:1), Foo(value: 2, key:2)]
        let current = [Foo(value: 2, key:2), Foo(value: 4, key:1)]
        let changes = current.calculateArrayChanges(previous)
        XCTAssert(changes.count == 1)
        XCTAssert(changes[0] == CollectionTypeChange.Move(from: 1, to: 0))
    }

    func testIdentifiableMoveAndUpdate() {
        let previous = [Foo(value: 2, key:1), Foo(value: 4, key:2)]
        let current = [Foo(value: 2, key:2), Foo(value: 4, key:1)]
        let changes = current.calculateArrayChanges(previous)
        XCTAssert(changes.count == 3)
        XCTAssert(changes[0] == CollectionTypeChange.Move(from: 1, to: 0))
        XCTAssert(changes[1] == CollectionTypeChange.Update(index: 0))
        XCTAssert(changes[2] == CollectionTypeChange.Update(index: 1))
    }

}