//
//  Foo.swift
//  RZCollectionType
//
//  Created by Brian King on 12/22/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

import RZCollectionType

struct Foo : Identifiable {
    let value: Int
    let key: Int

    func hasSameIdentity(other: Foo) -> Bool {
        return self.key == other.key
    }

}

func ==(lhs: Foo, rhs: Foo) -> Bool {
    return lhs.hasSameIdentity(rhs) && lhs.value == rhs.value
}

struct Room : Identifiable {
    let identifier: String
    let display: String

    func hasSameIdentity(other: Room) -> Bool {
        return self.identifier == other.identifier
    }
}

func ==(lhs: Room, rhs: Room) -> Bool {
    return lhs.hasSameIdentity(rhs) && lhs.display == rhs.display
}

enum FooterState {
    case More
    case Loading
    case End
}

enum TestRow : Identifiable {
    case RoomRow(Room: Room)
    case LoadMoreRow
    case Loading
    case Info(title: String)

    func hasSameIdentity(other: TestRow) -> Bool {
        switch (self, other) {
        case (let .RoomRow(roomA), let .RoomRow(roomB)):
            return roomA.hasSameIdentity(roomB)
        case (.LoadMoreRow, .LoadMoreRow):
            return true
        case (.Loading, .Loading):
            return true
        case (let .Info(titleA), let .Info(titleB)):
            return titleA == titleB
        default:
            return false
        }
    }
}

func ==(lhs: TestRow, rhs: TestRow) -> Bool {
    switch (lhs, rhs) {
    case (let .RoomRow(RoomA), let .RoomRow(RoomB)):
        return RoomA == RoomB
    case (let .Info(titleA), let .Info(titleB)):
        return titleA == titleB
    default:
        return lhs.hasSameIdentity(rhs)
    }
}

enum TestSection : Identifiable, CollectionType {
    case Info(name: String, rowTitle: String)
    case Group(name: String, rows: [Room])
    case Footer(state: FooterState)

    var startIndex: Int {
        return 0
    }

    var endIndex: Int {
        switch self {
        case .Info(_):
            return 1
        case .Footer(let state):
            return state == .End ? 0 : 1
        case .Group(_,let rows):
            return rows.count
        }
    }

    subscript(i: Int) -> TestRow {
        switch self {
        case .Info(_, let rowTitle):
            return TestRow.Info(title: rowTitle)
        case .Footer(let state):
            return state == .Loading ? TestRow.Loading : TestRow.LoadMoreRow
        case .Group(_,let rows):
            return TestRow.RoomRow(Room: rows[i])
        }
    }

    func hasSameIdentity(other: TestSection) -> Bool {
        switch (self, other) {
        case (let .Footer(stateA), let .Footer(stateB)):
            return stateA == stateB
        case (let .Info(nameA, _), let .Info(nameB, _)):
            return nameA == nameB
        case (let .Group(nameA, _), let .Group(nameB, _)):
            return nameA == nameB
        default:
            return false
        }
    }
}

func ==(lhs: TestSection, rhs: TestSection) -> Bool {
    switch (lhs, rhs) {
    case (let .Group(_, rowsA), let .Group(_, rowsB)):
        return lhs.hasSameIdentity(rhs) && rowsA == rowsB
    default:
        return lhs.hasSameIdentity(rhs)
    }
}
