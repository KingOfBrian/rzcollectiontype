//
//  RZCollectionTypeChangeSetTests.swift
//  RZCollectionType
//
//  Created by Brian King on 12/21/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import XCTest
@testable import RZCollectionType

class IndexSetTests: XCTestCase {
    let testDataA = [0,1,2,3,4,5,6,7,8,9,]

    func testPositiveShift() {
        var indexSet = IndexSet<Int>()
        indexSet.addIndexes(Range(start: 1, end: 3))
        indexSet.shiftUpIndexesStartingAtIndex(1)
        indexSet.shiftUpIndexesStartingAtIndex(3)
        indexSet.shiftUpIndexesStartingAtIndex(1)
        let indexes = indexSet.indexesInside(testDataA.indices)
        XCTAssert(indexes.count == 2)
        XCTAssert(indexSet.contains(3))
        XCTAssert(indexSet.contains(5))
    }

    func testNegativeShiftLeading() {
        var indexSet = IndexSet<Int>()
        indexSet.addIndexes(Range(start: 3, end: 5))
        // If the shift starts at an index that is contained, and the previous index is contained, grow the range
        indexSet.shiftDownIndexesStartingAtIndex(2)
        indexSet.shiftDownIndexesStartingAtIndex(2)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 2)
        XCTAssert(indexSet.contains(1))
        XCTAssert(indexSet.contains(2))

        // Shift Range does not enter indexSet
        // _ X X
        // -
        indexSet.shiftDownIndexesStartingAtIndex(0)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 2)
        XCTAssert(indexSet.contains(0))
        XCTAssert(indexSet.contains(1))
    }

    func testNegativeShiftHead() {
        // Shift Range specifies the start of the index range, it does not collapse any indexes
        // _ X X
        // _ -
        var indexSet = IndexSet<Int>()
        indexSet.addIndexes(Range(start: 1, end: 3))
        indexSet.shiftDownIndexesStartingAtIndex(1)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 2)
        XCTAssert(indexSet.contains(0))
        XCTAssert(indexSet.contains(1))
    }

    func testNegativeShiftTail() {
        // Shift lands inside the index set, trim the range
        // _ X X
        // _ _ -
        var indexSet = IndexSet<Int>()
        indexSet.addIndexes(Range(start: 1, end: 3))
        indexSet.shiftDownIndexesStartingAtIndex(2)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 1)
        XCTAssert(indexSet.contains(1))
    }

    func testNegativeShiftOntoTail() {
        // Shift lands inside the index set, trim the range
        // _ X X
        // _ _ _ -
        var indexSet = IndexSet<Int>()
        indexSet.addIndexes(Range(start: 1, end: 3))
        indexSet.shiftDownIndexesStartingAtIndex(3)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 1)
        XCTAssert(indexSet.contains(1))
        indexSet.shiftDownIndexesStartingAtIndex(2)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 0)
    }

    func testNegativeShiftOnStart() {
        // Shift Range below zero
        // X X
        // -
        var indexSet = IndexSet<Int>()
        indexSet.addIndexes(Range(start: 0, end: 2))
        indexSet.shiftDownIndexesStartingAtIndex(0)
        XCTAssert(indexSet.indexesInside(testDataA.indices).count == 1)
        XCTAssert(indexSet.contains(0))
    }
    
}
