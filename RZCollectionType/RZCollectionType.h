//
//  RZCollectionType.h
//  RZCollectionType
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RZCollectionType.
FOUNDATION_EXPORT double RZCollectionTypeVersionNumber;

//! Project version string for RZCollectionType.
FOUNDATION_EXPORT const unsigned char RZCollectionTypeVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RZCollectionType/PublicHeader.h>


