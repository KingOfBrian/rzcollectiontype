//
//  ChangeSet.swift
//  RZCollectionType
//
//  Created by Brian King on 12/19/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

struct CollectionTypeChangeSet<Index: RandomAccessIndexType> {
    private var inserts = IndexSet<Index>()
    private var updates = IndexSet<Index>()
    private var removes = IndexSet<Index>()

    mutating func insertAtIndex(index: Index) {
        updates.shiftUpIndexesStartingAtIndex(index)
        inserts.shiftUpIndexesStartingAtIndex(index)

        inserts.addIndex(index)
    }

    mutating func updateAtIndex(index: Index) {
        updates.addIndex(index)
    }

    mutating func removeAtIndex(index: Index) {
        let insertWasRemoved = inserts.contains(index)
        inserts.shiftDownIndexesStartingAtIndex(index)
        updates.shiftDownIndexesStartingAtIndex(index)
        if !insertWasRemoved {
            // If the index has been removed, add it to the end of the removed range
            var removeIndex = index
            while removes.contains(removeIndex) {
                removeIndex = removeIndex + 1
            }
            removes.addIndex(removeIndex)
        }
    }

    func shiftPreviousIndexToCurrent(previousIndex: Index, startIndex: Index) -> Index {
        var shiftedIndex = previousIndex
        for _ in removes.indexesStarting(startIndex).filter({$0 < previousIndex}) {
            shiftedIndex = shiftedIndex.predecessor()
        }
        return shiftedIndex
    }

    mutating func changes<U: CollectionType where
        U.Index == Index,
        U.Generator.Element: Identifiable
        > (previous: U, current: U) -> [CollectionTypeChange<Index>]
    {
        var changes: [CollectionTypeChange<Index>] = Array()

        // Consolidate moves. First obtain local, mutable copy
        for fromIndex in inserts.indexesInside(current.indices) {
            let obj = current[fromIndex]
            if let toIndex = previous.indexOf(obj.hasSameIdentity) where removes.contains(toIndex) {
                let shiftedToIndex = shiftPreviousIndexToCurrent(toIndex, startIndex: previous.startIndex)
                if shiftedToIndex != fromIndex {
                    changes.append(.Move(from:fromIndex, to: toIndex))
                }
                inserts.removeIndex(fromIndex)
                removes.removeIndex(toIndex)
            }
        }
        for index in removes.indexesInside(previous.indices) {
            changes.append(.Remove(index: index))
        }
        for index in inserts.indexesInside(current.indices) {
            changes.append(.Insert(index: index))
        }
        for index in updates.indexesInside(current.indices) {
            changes.append(.Update(index: index))
        }
        return changes
    }

    mutating func changes<U: CollectionType where
        U.Index == Index,
        U.Generator.Element: Equatable
        > (previous: U, current: U) -> [CollectionTypeChange<Index>]
    {
        var changes: [CollectionTypeChange<Index>] = Array()

        // Consolidate moves. First obtain local, mutable copy
        for fromIndex in inserts.indexesInside(current.indices) {
            let obj = current[fromIndex]
            if let toIndex = previous.indexOf(obj) where removes.contains(toIndex) {
                let shiftedToIndex = shiftPreviousIndexToCurrent(toIndex, startIndex: previous.startIndex)
                if shiftedToIndex != fromIndex {
                    changes.append(.Move(from:fromIndex, to: toIndex))
                }
                inserts.removeIndex(fromIndex)
                removes.removeIndex(toIndex)
            }
        }
        for index in removes.indexesInside(previous.indices) {
            changes.append(.Remove(index: index))
        }
        for index in inserts.indexesInside(current.indices) {
            changes.append(.Insert(index: index))
        }
        for index in updates.indexesInside(current.indices) {
            changes.append(.Update(index: index))
        }
        return changes
    }

}
