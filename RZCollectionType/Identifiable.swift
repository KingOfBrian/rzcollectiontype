//
//  Identifiable.swift
//  RZCollectionType
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

/**
 Protocol to determine if two objects have the same identity.
 
 NOTE: It would be possible to use the === operator for classes, but not value objects. I believe it's better to not support this approach as it's a bit obscure.
*/
public protocol Identifiable : Equatable {
    func hasSameIdentity(other: Self) -> Bool
}
