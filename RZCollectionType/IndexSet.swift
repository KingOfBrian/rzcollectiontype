//
//  IndexSet.swift
//  RZCollectionType
//
//  Created by Brian King on 12/21/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

struct IndexSet<Index: RandomAccessIndexType> {
    private var indexes = Array<Index>()

    /**
     Since RandomAccessIndexType can not check `>0`, force the user to 
     grab indices inside a bounds check. This will filter out any indices
     that were shifted into invalid index space.
     */
    func indexesInside(bounds: Range<Index>) -> Array<Index> {
        return indexes.filter() { index in
            return bounds.contains(index)
        }
    }
    func indexesStarting(start: Index) -> Array<Index> {
        return indexes.filter() { index in
            return index >= start
        }
    }

    func contains(index: Index) -> Bool {
        return indexes.contains(index)
    }

    mutating func addIndex(index: Index) {
        if !indexes.contains(index) {
            indexes.append(index)
        }
    }

    mutating func removeIndex(index: Index) {
        if let indexOfIndex = indexes.indexOf(index) {
            indexes.removeAtIndex(indexOfIndex)
        }
    }

    mutating func addIndexes(range: Range<Index>) {
        for index in range {
            self.addIndex(index)
        }
    }

    mutating func shiftDownIndexesStartingAtIndex(shiftIndex: Index) {
        // Remove any index that has been shifted on top of.
        let shiftStart = shiftIndex.predecessor()
        let possibleIndexOfStart = indexes.indexOf(shiftStart)
        if let indexOfStart = possibleIndexOfStart {
            indexes.removeAtIndex(indexOfStart)
        }
        // Iterate over every index after shiftIndex and replace it with it's predecessor
        for obj in indexes {
            let indexOfIndex = indexes.indexOf(obj)!
            if obj >= shiftStart {
                indexes.removeAtIndex(indexOfIndex)
                indexes.append(obj.predecessor())
            }
        }
    }

    mutating func shiftUpIndexesStartingAtIndex(shiftIndex: Index) {
        // Iterate over every index after shiftIndex and replace it with it's successor
        for obj in indexes {
            let indexOfIndex = indexes.indexOf(obj)!
            if obj >= shiftIndex {
                indexes.removeAtIndex(indexOfIndex)
                indexes.append(obj.successor())
            }
        }
    }
}
