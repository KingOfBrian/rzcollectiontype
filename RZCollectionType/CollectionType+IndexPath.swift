//
//  CollectionType+IndexPath.swift
//  RZCollectionType
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

extension CollectionType where
    Self.Generator.Element : CollectionType,
    Self.Index: RandomAccessIndexType,
    Self.Generator.Element.Index: RandomAccessIndexType
{
    func objectAtIndexPath(indexPath: NSIndexPath) -> Self.Generator.Element.Generator.Element {
        let sectionIndex = indexPath.indexAtPosition(0) as! Self.Index
        let rowIndex = indexPath.indexAtPosition(1) as! Self.Generator.Element.Index
        return self[sectionIndex][rowIndex]
    }
}
