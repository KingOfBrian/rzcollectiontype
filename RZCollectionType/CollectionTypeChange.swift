//
//  CollectionTypeChange.swift
//  RZCollectionType
//
//  Created by Brian King on 12/21/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

public enum CollectionTypeChange<Index: RandomAccessIndexType> : Equatable {
    case Insert(index: Index)
    case Update(index: Index)
    case Remove(index: Index)
    case Move(from: Index, to: Index)
}

public func ==<Index: RandomAccessIndexType>(lhs: CollectionTypeChange<Index>, rhs: CollectionTypeChange<Index>) -> Bool {
    switch (lhs, rhs) {
    case (let .Insert(indexA), let .Insert(indexB)):
        return indexA == indexB
    case (let .Update(indexA), let .Update(indexB)):
        return indexA == indexB
    case (let .Remove(indexA), let .Remove(indexB)):
        return indexA == indexB
    case (let .Move(fromA, toA), let .Move(fromB, toB)):
        return fromA == fromB && toA == toB
    default:
        return false
    }
}
