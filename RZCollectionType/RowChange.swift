//
//  Changes.swift
//  RZCollectionType
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

enum RowChange<SectionIndexType: RandomAccessIndexType, RowIndexType: RandomAccessIndexType> {
    case RowInsert(sectionIndex: SectionIndexType, rowIndex: RowIndexType)
    case RowUpdate(sectionIndex: SectionIndexType, rowIndex: RowIndexType)
    case RowRemove(sectionIndex: SectionIndexType, rowIndex: RowIndexType)
    case RowMove(fromSectionIndex: SectionIndexType, fromRowIndex: RowIndexType, toSectionIndex: SectionIndexType, toRowIndex: RowIndexType)

    static func fromArrayChange(sectionIndex: SectionIndexType, change: CollectionTypeChange<RowIndexType>) -> RowChange<SectionIndexType,RowIndexType> {
        switch change {
        case .Insert(let index):
            return .RowInsert(sectionIndex: sectionIndex, rowIndex:index)
        case .Update(let index):
            return .RowUpdate(sectionIndex: sectionIndex, rowIndex:index)
        case .Remove(let index):
            return .RowRemove(sectionIndex: sectionIndex, rowIndex:index)
        case .Move(let from, let to):
            return .RowMove(fromSectionIndex:sectionIndex, fromRowIndex:from,
                toSectionIndex:sectionIndex, toRowIndex:to)
        }
    }
}
