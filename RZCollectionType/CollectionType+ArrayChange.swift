//
//  CollectionType+ArrayChange.swift
//  RZCollectionType
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

/*
Generate changes when Element supports Identifiable. When this protocol is supported by the Element items can generate proper update events since it knows when an object has been changed.
*/
extension CollectionType where
    Self.Index: RandomAccessIndexType,
    Self.Generator.Element : Identifiable
{
    func calculateArrayChanges(previous: Self) -> [CollectionTypeChange<Self.Index>]
    {
        var changeSet: CollectionTypeChangeSet<Self.Index> = CollectionTypeChangeSet()
        // Iterate through both collections and determine removal events
        for obj in previous {
            let changePredicate = { obj.hasSameIdentity($0) }
            if self.indexOf(changePredicate) == nil {
                let previousIndex = previous.indexOf(obj)!
                changeSet.removeAtIndex(previousIndex)
            }
        }
        for obj in self {
            let index = self.indexOf(obj)!
            if let previousIndex = previous.indexOf({ obj.hasSameIdentity($0) }) {
                let shiftedPreviousIndex = previousIndex
                if (index != shiftedPreviousIndex) {
                    changeSet.removeAtIndex(previousIndex)
                }
            }
        }
        // Iterate through self and generate insert / update events.
        for obj in self {
            let index = self.indexOf({ obj.hasSameIdentity($0) })!
            if let previousIndex = previous.indexOf({ obj.hasSameIdentity($0) }) {
                if (index != previousIndex) {
                    changeSet.insertAtIndex(index)
                }
                let previous = previous[previousIndex]
                if obj != previous {
                    changeSet.updateAtIndex(index)
                }
            }
            else {
                changeSet.insertAtIndex(index)
            }
        }
        return changeSet.changes(previous, current: self)
    }
}


/*
Generate changes when Element supports Equatable. Update events can not be generated, since we don't know if the object changed or if it's a different object.
*/
extension CollectionType where
    Self.Index: RandomAccessIndexType,
    Self.Generator.Element : Equatable
{

    func calculateArrayChanges(previous: Self) -> [CollectionTypeChange<Self.Index>]
    {
        var changeSet: CollectionTypeChangeSet<Self.Index> = CollectionTypeChangeSet()
        // Iterate through both collections and determine removal events
        for obj in previous {
            let index = self.indexOf(obj)
            if index == nil {
                changeSet.removeAtIndex(previous.indexOf(obj)!)
            }
        }
        for obj in self {
            let index = self.indexOf(obj)!
            if let previousIndex = previous.indexOf(obj) {
                if (index != previousIndex) {
                    changeSet.removeAtIndex(previousIndex)
                }
            }
        }
        // Iterate through self and generate insert events.
        for obj in self {
            let index = self.indexOf(obj)!
            if let previousIndex = previous.indexOf(obj) {
                if (index != previousIndex) {
                    changeSet.insertAtIndex(index)
                }
            }
            else {
                changeSet.insertAtIndex(index)
            }
        }
        return changeSet.changes(previous, current: self)
    }
}

