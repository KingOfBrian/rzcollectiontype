//
//  CollectionType+SectionChange.swift
//  RZCollectionType
//
//  Created by Brian King on 12/18/15.
//  Copyright © 2015 Raizlabs. All rights reserved.
//

import Foundation

/*
This file generates changes between 2 nested objects that support CollectionType.
The objects contained by the first CollectionType will be called Section objects.
The objects contained by the second CollectionType will be called Row objects.

There are a number of implementations depending on what the Row and Section objects support.
*/

/*
Section change calculation when only the Row object supportes Identifiable.
NOTE: This exact function can work if the Row object supports Equatable, but I can't figure out how to tell the type system this.
*/
extension CollectionType where
    Self.Generator.Element : CollectionType,
    Self.Index: RandomAccessIndexType,
    Self.Generator.Element.Index: RandomAccessIndexType,
    Self.Generator.Element.Generator.Element: Equatable
{
    func calculateSectionedArrayChanges(previousCollectionType: Self)
        -> ([CollectionTypeChange<Self.Index>], [RowChange<Self.Index, Self.Generator.Element.Index>])
    {
        var sectionChanges: [CollectionTypeChange<Self.Index>] = Array()
        var rowChanges: [RowChange<Self.Index, Self.Generator.Element.Index>] = Array()
        let count = max(self.count, previousCollectionType.count)
        for i in self.startIndex..<self.startIndex.advancedBy(count) {
            let has = self.indices.contains(i)
            let had = previousCollectionType.indices.contains(i)
            switch (has, had) {
            case (true, true):
                for change in self[i].calculateArrayChanges(previousCollectionType[i]) {
                    rowChanges.append(RowChange.fromArrayChange(i, change: change))
                }
            case (false, true):
                sectionChanges.append(.Remove(index:i))
            case (true, false):
                sectionChanges.append(.Insert(index:i))
            case (false, false):
                fatalError("There is no way this should happen.")
            }
        }

        return (sectionChanges, rowChanges)
    }
}

/*
Section change calculation for when the Section object supports Identifiable.
*/
extension CollectionType where
    Self.Generator.Element : CollectionType,
    Self.Generator.Element : Identifiable,
    Self.Index: RandomAccessIndexType,
    Self.Generator.Element.Index: RandomAccessIndexType,
    Self.Generator.Element.Generator.Element: Identifiable
{
    func calculateSectionedArrayChanges(previousCollectionType: Self)
        -> ([CollectionTypeChange<Self.Index>], [RowChange<Self.Index, Self.Generator.Element.Index>])
    {
        let sectionChanges = self.calculateArrayChanges(previousCollectionType)
        var rowChanges: [RowChange<Self.Index, Self.Generator.Element.Index>] = Array()

        for change in sectionChanges {
            switch change {
            case .Update(let index):
                let currentSection = self[index]
                guard let previousSectionIndex = previousCollectionType.indexOf({currentSection.hasSameIdentity($0)}) else {
                    break
                }
                let previousSection = previousCollectionType[previousSectionIndex]
                let changes = currentSection.calculateArrayChanges(previousSection)
                for change in changes {
                    rowChanges.append(RowChange.fromArrayChange(index, change: change))
                }
            default:
                break
            }
        }

        return (sectionChanges, rowChanges)
    }
}
